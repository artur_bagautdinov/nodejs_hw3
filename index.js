const express = require('express');
const mongoose = require('mongoose');
const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

const app = express();
const port = process.env.PORT || 8080;


app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/trucks', truckRouter);
app.use('/api/loads', loadRouter);


const start = async () => {
  await mongoose.connect('mongodb+srv://testuser:testuser1@cluster0.g8c2f.mongodb.net/homework3?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(port, () => {
    console.log(`Server is running on ${port}!`);
  });
};

start();


