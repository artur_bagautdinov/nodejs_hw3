const express = require('express');
const router = new express.Router();

const {
  login,
  registration,
  forgotPassword,
} = require('../controllers/authController');
const {validateRegistration} = require('../middlewares/validateRegistration');
const {asyncWrapper} = require('./helpers');


router.post(
    '/register',
    asyncWrapper(validateRegistration),
    asyncWrapper(registration),
);

router.post('/login', asyncWrapper(login));
router.post('/forgot_password', asyncWrapper(forgotPassword));


module.exports = router;
