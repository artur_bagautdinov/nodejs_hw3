const express = require('express');
const router = new express.Router();
const {nanoid} = require('nanoid');
const {User} = require('../models/userModel');
const {Load} = require('../models/loadModel');
const {authMiddleware} = require('../middlewares/authMiddleware');

router.get('/', authMiddleware, async (req, res) => {
  try {
    const {userAuthData} = req;
    const _id = userAuthData._id;

    const user = await User.findById({_id});

    if (user.role !== 'SHIPPER') {
      return res.status(400).json(
          {message: 'Action is forbidden for this role!'},
      );
    }

    const loads = await User.find(
        {_id},
        {loads: 1, _id: 0},
    );

    res.json(loads[0]);
  } catch (e) {
    res.status(500).json({message: e.message});
  }
});


router.post('/', authMiddleware, async (req, res) => {
  try {
    const {userAuthData} = req;
    const _id = userAuthData._id;


    const user = await User.findById({_id});

    if (user.role !== 'SHIPPER') {
      return res.status(400).json(
          {message: 'Action is forbidden for this role!'},
      );
    }

    user.schema.add({loads: []});


    const load = new Load({
      _id: await nanoid(),
      created_by: user._id,
      name: req.body.name,
      payload: req.body.payload,
      pickup_address: req.body.pickup_address,
      delivery_address: req.body.delivery_address,
      dimensions: {
        width: req.body.dimensions.width,
        length: req.body.dimensions.length,
        height: req.body.dimensions.height,
      },
    });


    await User.updateOne(
        {_id},
        {$push: {loads: load}},
    );

    res.json({message: 'Load created successfully'});
  } catch (e) {
    res.status(500).json({message: e.message});
  }
});


module.exports = router;
