const express = require('express');
const router = new express.Router();
const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');
const {authMiddleware} = require('../middlewares/authMiddleware');
const saltRounds = 10;

router.get('/', authMiddleware, async (req, res) => {
  try {
    const {userAuthData} = req;
    const userID = userAuthData._id;

    const user = await User.findOne(
        {_id: userID},
        {
          _id: 1,
          email: 1,
          created_date: 1,
        },
    );

    if (!user) {
      return res.status(400).json({message: 'User not found!'});
    }

    res.json({user});
  } catch (e) {
    res.status(500).json({message: e.message});
  }
});


router.delete('/', authMiddleware, async (req, res)=>{
  try {
    const {userAuthData} = req;
    const userID = userAuthData._id;

    const user = await User.findOne({_id: userID});

    if (user.role !== 'DRIVER') {
      await User.findByIdAndDelete({_id: userID});
      res.json({message: 'Profile deleted successfully'});
    } else {
      res.status(400).json(
          {message: 'Action is forbidden for this role!'},
      );
    }
  } catch (e) {
    res.status(500).json({message: e.message});
  }
});

router.patch('/password', authMiddleware, async (req, res)=> {
  try {
    const {oldPassword, newPassword} = req.body;
    const {userAuthData} = req;
    const userID = userAuthData._id;

    const user = await User.findById({_id: userID});
    const userPassword = user.password;

    if (! await bcrypt.compare(oldPassword, userPassword)) {
      return res.status(400).json(
          {message: 'Old password is incorrect!'},
      );
    }

    await User.findByIdAndUpdate(
        {_id: userID},
        {password: await bcrypt.hash(newPassword, saltRounds)},
    );

    res.json({message: 'Password changed successfully'});
  } catch (e) {
    res.status(500).json({message: e.message});
  }
});


module.exports = router;
