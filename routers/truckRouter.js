const express = require('express');
const router = new express.Router();
const {nanoid} = require('nanoid');
const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');
const {authMiddleware} = require('../middlewares/authMiddleware');
const {TRUCK_TYPES} = require('../utils');


router.get('/', authMiddleware, async (req, res) => {
  try {
    const {userAuthData} = req;
    const _id = userAuthData._id;

    const user = await User.findById({_id});

    if (user.role !== 'DRIVER') {
      return res.status(400).json(
          {message: 'Action is forbidden for this role!'},
      );
    }

    const trucks = await User.find(
        {_id},
        {trucks: 1, _id: 0},
    );

    res.json(trucks[0]);
  } catch (e) {
    res.status(500).json({message: e.message});
  }
});


router.post('/', authMiddleware, async (req, res) => {
  try {
    const {type} = req.body;
    const {userAuthData} = req;
    const _id = userAuthData._id;

    const truckData = TRUCK_TYPES.find((item) => item.type === type);

    if (!truckData) {
      return res.status(400).json(
          {message: 'Truck type specified is not correct!'},
      );
    }

    const user = await User.findOne({_id});

    if (user.role !== 'DRIVER') {
      return res.status(400).json(
          {message: 'Action is forbidden for this role!'},
      );
    }

    user.schema.add({trucks: []});


    const truck = new Truck({
      _id: await nanoid(),
      created_by: user._id,
      type: truckData.type,
    });


    await User.updateOne(
        {_id},
        {$push: {trucks: truck}},
    );

    res.json({message: 'Truck created successfully'});
  } catch (e) {
    res.status(500).json({message: e.message});
  }
});

router.get('/:id', authMiddleware, async (req, res) => {
  try {
    const {userAuthData} = req;
    const truckId = req.params.id;
    const userid = userAuthData._id;

    const user = await User.findById({_id: userid});


    if (user.role !== 'DRIVER') {
      return res.status(400).json(
          {message: 'Action is forbidden for this role!'},
      );
    }


    /**
       *  here is below the main  blocker of this homework.
          I couldn't retrive value of array *trucks* in collection
          and didn't find any solution and couldn't proceed
          without getting this data
       */

    const truckData = await User.findOne(
        {_id: userid},
        {'trucks': {
          $elemMatch: {'_id': truckId},
        },
        '_id': 0,
        },
    );


    res.json(truckData);
  } catch (e) {
    res.status(500).json({message: e.message});
  }
});

module.exports = router;
