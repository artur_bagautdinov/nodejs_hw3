const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');


module.exports.authMiddleware = (req, res, next)=>{
  const authorizationHeader = req.headers['authorization'];

  if (!authorizationHeader) {
    return res.status(401).json(
        {message: `No Authorization http header found!`},
    );
  }


  const token = authorizationHeader;

  if (!token) {
    return res.status(401).json(
        {message: 'No JWT token found!'},
    );
  }

  req.userAuthData = jwt.verify(token, JWT_SECRET);

  next();
};
