const Joi = require('joi');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().required(),
    password: Joi.string().pattern(
        new RegExp('^[a-zA-Z0-9]{3,30}$'),
    ).required(),
    role: Joi.string().valid('SHIPPER', 'DRIVER').required(),
  });


  try {
    await schema.validateAsync({
      email: req.body.email,
      password: req.body.password,
      role: req.body.role,
    });
  } catch (e) {
    req.body.errorMessage = e.message;
  }

  next();
};
