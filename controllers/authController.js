const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const generator = require('generate-password');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');
const saltRounds = 10;

module.exports.registration = async (req, res) => {
  const {email, password, role, errorMessage} = req.body;

  try {
    if (errorMessage) {
      return res.status(400).json(
          {message: errorMessage},
      );
    }
    const uniqueUser = await User.find({email});

    if (uniqueUser.length > 0) {
      return res.status(400).json(
          {message: 'This email is already existing!'},
      );
    }

    const user = new User({
      email,
      password: await bcrypt.hash(password, saltRounds),
      role,
    });

    await user.save();

    res.json({message: 'Profile created successfully'});
  } catch (e) {
    res.status(500).json({message: `Internal server error`});
  }
};


module.exports.login = async (req, res) => {
  try {
    const {email, password} = req.body;

    const user = await User.findOne({email});

    if (!user) {
      return res.status(400).json(
          {message: `No user with username '${username}' found!`},
      );
    }

    if (!await bcrypt.compare(password, user.password)) {
      return res.status(400).json({message: `Wrong password!`});
    }

    const token = jwt.sign(
        {email: user.email, _id: user._id},
        JWT_SECRET,
    );

    res.json({jwt_token: token});
  } catch (e) {
    res.status(500).json({message: `Internal server error`});
  }
};


module.exports.forgotPassword = async (req, res) => {
  try {
    const {email} = req.body;

    const user = await User.findOne({email});

    if (!user) {
      return res.status(400).json(
          {message: `No user with this email found!`},
      );
    }

    const _id = user._id;
    const newPassword = generator.generate({
      length: 5,
      numbers: true,
    });

    await User.findByIdAndUpdate(
        {_id},
        {password: await bcrypt.hash(newPassword, 10)},
    );

    res.json({message: 'New password sent to your email address'});
  } catch (e) {
    res.status(500).json({message: `Internal server error`});
  }
};

