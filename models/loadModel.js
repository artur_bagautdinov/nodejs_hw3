const mongoose = require('mongoose');
const {Schema} = mongoose;

const loadSchema = new Schema({
  _id: {
    type: String,
    required: true,
  },
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    required: true,
    default: null,
  },
  status: {
    type: String,
    required: true,
    default: 'NEW',
  },
  state: {
    type: String,
    required: true,
    default: 'En route to Pick Up',
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [{message: String, time: Date}],
  created_date: {
    type: Date,
    default: Date.now(),
  },

});


module.exports.Load = mongoose.model('Load', loadSchema);
